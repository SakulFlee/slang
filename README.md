SLANG
---

SLANG is a unique programming language, developed by @Sakul6499.  
SLANG source files can be compiled into other languages.
(Supported targets are listed below)

This repository includes the current SLANG-Compiler.

Current State
---
The current attempt is to automatically compile the generated C++ source, to produce a C++ Binary.  
In addition a "C++ Cross-Platform Library" is build.


Targets
---
| Target        | State         | 
| ------------- | ------------: |
| Literal AST   | **working**   |
| Encoded AST   | **working**   |
| C++ Source    | **working**   |
| C++ Binary    | planned       |
| SLANG Binary  | planned       | 
| Java Source   | planned       |
| Java Binary   | planned       |
| C# Source     | planned       |
| C# Binary     | planned       |

Changelog
---
* v0.5 [**planned**]
    * 'real functions'
* v0.4 [**planned**]
    * variable declaration
* v0.3 [**planned**]
    * class declaration
* v0.2 [**current**]
    * nearly everything got recoded
    * basic functionally working:
        * function declaration's
        * source syntax available
        * SLANG parser
        * SLANG to AST transformer
        * AST to C++ transformer
* v0.1 [never released]
    * basic concept is working
    
License
--- 
The SLANG project currently is licensed by the **GNU AFFERO GENERAL PUBLIC LICENSE, Version 3, 19 November 2007**.
The full license file is included within the source.

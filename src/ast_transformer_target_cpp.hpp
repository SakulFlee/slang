//
// Created by Lukas on 02.06.2017.
//

#ifndef SLANG_AST_TRANSFORMER_TARGET_CPP_HPP
#define SLANG_AST_TRANSFORMER_TARGET_CPP_HPP


#include "ast_transformer_template.hpp"

namespace slang {
    namespace ast_transformer {
        class ast_transformer_target_cpp : ast_transformer_template {
        public:
            ast_transformer_target_cpp(const Vector<slang::model::ast::ASTObject> &source);

            String transform() override;
        };
    }
}


#endif //SLANG_AST_TRANSFORMER_TARGET_CPP_HPP

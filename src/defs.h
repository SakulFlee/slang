/*
 * SLANG - Test (Test.cpp)
 * Copyright (C) 2017 Lukas | @Sakul6499 [Sakul6499@live.de]
 * Created on 29.05.2017.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Default definitions.
 * Contains:
 *  - (library) includes
 *  - helper definitions
 *  - type definitions (for easier coding)
 */
#ifndef SLANG_DEFS_H
#define SLANG_DEFS_H

/* (library) includes */
// Strings
#include <string>
// IOStreams [std::cout; std::endl]
#include <iostream>
// File Streams
#include <fstream>
// Lists
#include <list>
// Vector
#include <vector>
// Map
#include <map>

/* helper definitions */
/**
 * prints the given message msg to std::cout
 */
#define print(msg) std::cout << msg;
/**
 * prints the given message msg to std::cout and end line [std::endl]
 */
#define println(msg) std::cout << msg << std::endl;
/**
 * prints an empty space character to std::cout
 */
#define empty std::cout << " ";
/**
 * prints an empty space character to std::cout and end line [std::endl]
 */
#define emptyln std::cout << " " << std::endl;
/**
 * prints a debug message to std::cout and end line
 * Example: #<input> -> <output>
 */
#define debug(msg) std::cout << #msg << " -> " << msg << std::endl;

/**
 * Type definition from type std::string to String for easier coding
 */
typedef std::string String;
/**
 * Type definition from type std::ifstream to FileInputStream for easier coding
 */
typedef std::ifstream FileInputStream;
/**
 * Type definition from type std::ofstream to FileOutputStream for easier coding
 */
typedef std::ofstream FileOutputStream;
/**
 * Type definition from type std::list<type> to List for easier coding
 */
template<typename type> using List = std::list<type>;
/**
 * Type definition from type std::list<type> to List for easier coding
 */
template<typename type> using Vector = std::vector<type>;
/**
 * Type definition from type std::map<firstType, secondType> to List for easier coding
 */
template<typename firstType, typename secondType> using Map = std::map<firstType, secondType>;
/**
 * Type definition from type std::pair<firstType, secondType> to List for easier coding
 */
template<typename firstType, typename secondType> using Pair = std::pair<firstType, secondType>;

#endif //SLANG_DEFS_H

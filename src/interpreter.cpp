/*
 * SLANG - Test (Test.cpp)
 * Copyright (C) 2017 Lukas | @Sakul6499 [Sakul6499@live.de]
 * Created on 29.05.2017.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "interpreter.h"

slang::interpreter::interpreter() : slang::interpreter::interpreter(String(0, '\0')) {
    // Set source to zero terminated String to prevent 'Garbage'
}

slang::interpreter::interpreter(String sourceInput) {
    setSource(sourceInput);
}

void slang::interpreter::saveSourceFromStream(FileInputStream &sourceInputStream) {
    saveSourceFromStream(sourceInputStream, false);
}

slang::interpreter::interpreter(FileInputStream sourceInputStream) {
    saveSourceFromStream(sourceInputStream);
}

void slang::interpreter::saveSourceFromStream(FileInputStream &sourceInputStream, bool closeAfter) {
    FileInputStream::pos_type fileSize = sourceInputStream.tellg();
    sourceInputStream.seekg(0, std::ios::beg);

    source = String((unsigned long long int) fileSize, '\0');
    sourceInputStream.read(&source[0], fileSize);

    if (closeAfter) sourceInputStream.close();
}

void slang::interpreter::setSource(const String &source) {
    interpreter::source = source;
}

slang::interpreter::~interpreter() {
    source.clear();
}

Vector<ASTObject> slang::interpreter::parse() {
    interpreter_parser parser(source);
    return parser.parse();
}

ASTType slang::interpreter::checkLabel(String word) {
    // TODO
    return ASTType::UNDEFINED;
}

void slang::interpreter::processASTObject(ASTObject astObject) {
    // TODO
}

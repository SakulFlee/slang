/*
 * SLANG - Test (Test.cpp)
 * Copyright (C) 2017 Lukas | @Sakul6499 [Sakul6499@live.de]
 * Created on 29.05.2017.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SLANG Interpreter class.
 * Translates SLANG-Source-Syntax (SSS) to an Abstract-Syntax-Tree (AST).
 */
#ifndef SLANG_INTERPRETER_H
#define SLANG_INTERPRETER_H

// Default Includes
#include "defs.h"

// Model AST
#include "model_ast.hpp"

// Seperated parser
#include "interpreter_parser.cpp"

/**
 * Default SLANG namespace
 */
namespace slang {
    class interpreter {
    public:
        /**
         * Default constructor (empty)
         */
        interpreter();

        /**
         * Secondary constructor
         * Used for directly setting the source input by String.
         *
         * @param sourceInput the slang source to be parsed.
         */
        explicit interpreter(String sourceInput);

        /**
         * Secondary constructor
         * Used for directly setting the source input by FileInputStream.
         *
         * @param sourceInputStream the slang source to be pasrsed.
         */
        explicit interpreter(FileInputStream sourceInputStream);

        /**
         * Deconstructor
         * Used to deconstrudt interpreter class.
         * [Free up memory -> unload source]
         */
        virtual ~interpreter();

        /**
         * Method to save source from a FileInputStream.
         * The source input stream will be read and the result will be stored as the source input.
         *
         * @param sourceInputStream a FileInputStream, containing the slang source [> Input].
         * @param closeAfter a Boolean, where 'true' indicates to close the input stream afterwards.
         */
        void saveSourceFromStream(FileInputStream &sourceInputStream, bool closeAfter);

        /**
         * Method to save source from FileInputStream.
         * The source input stream will be read and the result will be stored as the source input.
         *
         * The source input stream will be closed by default.
         *
         * @param sourceInputStream a FileInputStream, containing the slang source [> Input].
         */
        void saveSourceFromStream(FileInputStream &sourceInputStream);

        Vector<ASTObject> parse();

        ASTType checkLabel(String word);

        void processASTObject(ASTObject astObject);

        /**
         * [Setter]
         * Will set the source [> Input]
         *
         * @param source a String, containing the source [> Input]
         */
        void setSource(const String &source);

    private:
        /**
         * The source input, which will be parsed.
         */
        String source;
    };
}

/**
 * Type definition for easier usage.
 */
typedef slang::interpreter SLANGInterpreter;

#endif //SLANG_INTERPRETER_H

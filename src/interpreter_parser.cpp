/*
 * SLANG - interpreter_parser (interpreter_parser.cpp) 
 * Copyright (C) 2017 Lukas | @Sakul6499 [Sakul6499@live.de]
 * Created on 01.06.2017.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "interpreter_parser.hpp"

slang::interpreter_parser::interpreter_parser(String source) : source(source) {

}

Vector<ASTObject> slang::interpreter_parser::parse() {
    if (source.size() <= 0) {
        println("Error: Source is empty!");
        exit(-1);
    }

    ParserMode mode = ParserMode::NORMAL_MODE;
    object = {};
    extraObject = {};
    result = Vector<ASTObject>();
    int char_index_1 = 0;
    int char_index_2 = 0;
    ParserMode subMode = ParserMode::NORMAL_MODE;

    for (String::size_type i = 0; i < source.size(); i++) {
        char *c = &source[i];

        switch (mode) {
            case model::interpreter_parser_modes::NORMAL_MODE: {
                bool wordEnd = false;
                if (*c == ' ' || *c == '\t') {
                    wordEnd = true;
                } else if (*c == '\n' || *c == '\r') {
                    wordEnd = true;
                } else if (*c == '/') {
                    char *_c = &source[++i];
                    if (*_c == '/') {
                        mode = ParserMode::SINGLE_LINE_COMMENT_MODE;
                        subMode = ParserMode::NORMAL_MODE;
                        wordEnd = true;
                    } else if (*_c == '*') {
                        mode = ParserMode::MULTI_LINE_COMMENT_MODE;
                        subMode = ParserMode::NORMAL_MODE;
                        wordEnd = true;
                    } else {
                        extraObject = {ASTType::CHAR_DIVISION};
                        wordEnd = true;
                    }
                } else if (*c == '\"') {
                    mode = ParserMode::STRING_MODE;
                    subMode = ParserMode::NORMAL_MODE;
                    wordEnd = true;
                } else if (*c == '@') {
                    String::size_type _i = i;

                    char *_c = &source[++i];
                    while (*_c == ' ') {
                        if (i < source.size())
                            _c = &source[++i];
                    }

                    if (*_c == '{') {
                        mode = ParserMode::SOURCE_MODE;
                        subMode = ParserMode::NORMAL_MODE;
                        char_index_1 = 1;
                        wordEnd = true;
                    } else {
                        i = _i;
                        object.value += *c;
                    }
                } else if (*c == '{') {
                    extraObject = {ASTType::CHAR_CURLY_BRACKET_OPEN};
                    wordEnd = true;
                } else if (*c == '}') {
                    extraObject = {ASTType::CHAR_CURLY_BRACKET_CLOSE};
                    wordEnd = true;
                } else if (*c == '(') {
                    extraObject = {ASTType::CHAR_ROUND_BRACKET_OPEN};
                    wordEnd = true;
                } else if (*c == ')') {
                    extraObject = {ASTType::CHAR_ROUND_BRACKET_CLOSE};
                    wordEnd = true;
                } else if (*c == ';') {
                    extraObject = {ASTType::CHAR_SEMICOLON};
                    wordEnd = true;
                } else if (*c == '.') {
                    extraObject = {ASTType::CHAR_DOT};
                    wordEnd = true;
                } else if (*c == ',') {
                    extraObject = {ASTType::CHAR_COMMA};
                    wordEnd = true;
                } else if (*c == ':') {
                    extraObject = {ASTType::CHAR_COLON};
                    wordEnd = true;
                } else if (*c == '+') {
                    extraObject = {ASTType::CHAR_ADDITION};
                    wordEnd = true;
                } else if (*c == '-') {
                    extraObject = {ASTType::CHAR_SUBSTRACTION};
                    wordEnd = true;
                } else if (*c == '*') {
                    extraObject = {ASTType::CHAR_MULTIPLICATION};
                    wordEnd = true;
                } else {
                    object.value += *c;
                }

                if (wordEnd) {
                    if (object.type == ASTType::UNDEFINED) {
                        if (object.value.size() > 0) {
                            processObject(object);
                        }
                    } else {
                        processObject(object);
                    }

                    if(extraObject.type == ASTType::UNDEFINED) {
                        if(extraObject.value.size() > 0) {
                            processObject(extraObject);
                        }
                    } else {
                        processObject(extraObject);
                    }
                }

                break;
            }
            case model::interpreter_parser_modes::SINGLE_LINE_COMMENT_MODE: {
                switch(subMode) {
                    case model::interpreter_parser_modes::NORMAL_MODE:{
                        if(*c == '\n' || *c == '\r') {
                            mode = ParserMode::NORMAL_MODE;
                            object.type = ASTType::SINGLE_LINE_COMMENT;
                            processObject(object);

                            continue;
                        }
                        object.value += *c;

                        break;
                    }
                    default: {
                        println("Error: Invalid mode within parser - single line comment mode detected! [" << subMode << "]");
                        exit(-1);
                    }
                }
                break;
            }
            case model::interpreter_parser_modes::MULTI_LINE_COMMENT_MODE: {
                switch(subMode) {
                    case model::interpreter_parser_modes::NORMAL_MODE:{
                        if(*c == '*') {
                            char *_c = &source[++i];
                            if(*_c == '/') {
                                mode = ParserMode::NORMAL_MODE;
                                object.type = ASTType::MULTI_LINE_COMMENT;
                                processObject(object);

                                continue;
                            } else {
                                object.value += *c;
                                object.value += *_c;
                            }

                            continue;
                        }

                        object.value += *c;
                        break;
                    }
                    default: {
                        println("Error: Invalid mode within parser - multi line comment mode detected! [" << subMode << "]");
                        exit(-1);
                    }
                }
                break;
            }
            case model::interpreter_parser_modes::STRING_MODE: {
                switch(subMode) {
                    case model::interpreter_parser_modes::NORMAL_MODE:{
                        if(*c == '"') {
                            char *_c = &source[i - 1];
                            if(*_c != '\\') {
                                mode = ParserMode::NORMAL_MODE;
                                object.type = ASTType::STRING;
                                processObject(object);
                            }
                        }

                        object.value += *c;
                        break;
                    }
                    default: {
                        println("Error: Invalid mode within parser - string mode detected! [" << subMode << "]");
                        exit(-1);
                    }
                }
                break;
            }
            case model::interpreter_parser_modes::SOURCE_MODE: {
                switch (subMode) {
                    case model::interpreter_parser_modes::NORMAL_MODE: {
                        if (*c == '\t') *c = ' ';

                        if (*c == ' ' && (object.value.back() == '\n' || object.value.back() == '\r' ||
                                          object.value.size() == 0))
                            continue;

                        if ((*c == '\n' || *c == '\r') && object.value.size() == 0)
                            continue;

                        if (*c == '/') {
                            char *_c = &source[++i];
                            if (*_c == '/') {
                                subMode = ParserMode::SINGLE_LINE_COMMENT_MODE;
                                object.value += *c;
                                object.value += *_c;
                            } else if (*_c == '*') {
                                subMode = ParserMode::MULTI_LINE_COMMENT_MODE;
                                object.value += *c;
                                object.value += *_c;
                            } else {
                                object.value += *c;
                                i--;
                            }

                            continue;
                        }

                        if (*c == '\"') {
                            subMode = ParserMode::STRING_MODE;
                            char_index_2++;

                            object.value += *c;
                            continue;
                        }

                        if (*c == '{') {
                            char_index_1++;
                        }

                        if (*c == '}') {
                            if (--char_index_1 == 0) {
                                while (object.value.back() == '\n' || object.value.back() == '\r' ||
                                       object.value.back() == '\t' || object.value.back() == ' ') {
                                    object.value.pop_back();
                                }

                                object.type = ASTType::SOURCE;
                                processObject(object);

                                mode = ParserMode::NORMAL_MODE;
                                continue;
                            }
                        }

                        object.value += *c;
                        break;
                    }
                    case model::interpreter_parser_modes::SINGLE_LINE_COMMENT_MODE: {
                        object.value += *c;
                        if (*c == '\n' || *c == '\r') {
                            subMode = ParserMode::NORMAL_MODE;
                        }
                        break;
                    }
                    case model::interpreter_parser_modes::MULTI_LINE_COMMENT_MODE: {
                        object.value += *c;
                        if (*c == '*') {
                            char *_c = &source[++i];
                            if (*_c == '/') {
                                subMode = ParserMode::NORMAL_MODE;
                            }
                        }
                        break;
                    }
                    case model::interpreter_parser_modes::STRING_MODE: {
                        object.value += *c;

                        if (*c == '\"') {
                            char_index_2--;
                            if (char_index_2 == 0) {
                                subMode = ParserMode::NORMAL_MODE;
                            }
                        }
                        break;
                    }
                    default: {
                        println("Error: Invalid mode within parser - source mode detected! [" << subMode << "]");
                        exit(-1);
                    }
                }
                break;
            }
        }
    }

    return result;
}

void slang::interpreter_parser::processObject(ASTObject &astObject) {
    /* normal ast object */
    if (astObject.type == ASTType::UNDEFINED) {
        ASTType qualifier = checkWordForASTQualifier();
        if (qualifier != ASTType::UNDEFINED) {
            astObject.type = qualifier;
        } else {
            astObject.type = ASTType::LABEL;
        }
    }

    result.push_back(astObject);
    astObject = {};
};

ASTType slang::interpreter_parser::checkWordForASTQualifier() {
    if (object.value == "fun")
        return ASTType::INDICATOR_FUNCTION;

    if (object.value == "cls")
        return ASTType::INDICATOR_CLASS;

    return ASTType::LABEL;
}

/*
 * SLANG - interpreter_parser (interpreter_parser.hpp) 
 * Copyright (C) 2017 Lukas | @Sakul6499 [Sakul6499@live.de]
 * Created on 01.06.2017.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SLANG_INTERPRETER_PARSER_HPP
#define SLANG_INTERPRETER_PARSER_HPP

#include "defs.h"
#include "model_ast.hpp"
#include "settings.h"
#include "model_interpreter_parser_modes.hpp"

namespace slang {
    class interpreter_parser {
    public:
        explicit interpreter_parser(String source);

        Vector<ASTObject> parse();

    private:
        void processObject(ASTObject &astObject);

        ASTType checkWordForASTQualifier();

    private:
        String source;
        Vector<ASTObject> result;

        ASTObject object;
        ASTObject extraObject;
    };
}

#endif //SLANG_INTERPRETER_PARSER_HPP

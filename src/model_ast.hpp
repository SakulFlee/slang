/*
 * SLANG - model_ast (model_ast.hpp) 
 * Copyright (C) 2017 Lukas | @Sakul6499 [Sakul6499@live.de]
 * Created on 31.05.2017.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Model AST definition
 */
#ifndef SLANG_MODEL_AST_HPP
#define SLANG_MODEL_AST_HPP

/**
 * Default SLANG namespace
 */
namespace slang {
    /**
     * Model namespace
     */
    namespace model {
        /**
         * AST namespace
         */
        namespace ast {
            /**
             * ASTTypes enum
             * Includes all (valid) AST Types.
             * Note: Encoded AST makes use of order
             */
            typedef enum ASTTypes {
                /* Default */
                /**
                 * Undefined value [default]
                 */
                        UNDEFINED = 0,

                /* Special Types */
                /**
                 * Single-Line comment
                 */
                        SINGLE_LINE_COMMENT,
                /**
                 * Multi-Line comment
                 */
                        MULTI_LINE_COMMENT,
                /**
                 * String
                 */
                        STRING,
                /**
                 * Source Type
                 * [Native Code, which just get's copied]
                 */
                        SOURCE,

                /* Special Chars */
                /**
                 * [Char]
                 * Curly Bracket Open
                 */
                        CHAR_CURLY_BRACKET_OPEN,
                /**
                 * [Char]
                 * Curly Bracket Close
                 */
                        CHAR_CURLY_BRACKET_CLOSE,
                /**
                 * [Char]
                 * Round Bracket Open
                 */
                        CHAR_ROUND_BRACKET_OPEN,
                /**
                 * [Char]
                 * Round Bracket Close
                 */
                        CHAR_ROUND_BRACKET_CLOSE,
                /**
                 * [Char]
                 * Semicolon
                 */
                        CHAR_SEMICOLON,
                /**
                 * [Char]
                 * Dot
                 */
                        CHAR_DOT,
                /**
                 * [Char]
                 * Comma
                 */
                        CHAR_COMMA,

                /**
                 * [Char]
                 * Colon
                 */
                        CHAR_COLON,

                /* Mathematical operator */
                /**
                 * [Char]
                 * Addition
                 */
                        CHAR_ADDITION,
                /**
                 * [Char]
                 * Substraction
                 */
                        CHAR_SUBSTRACTION,
                /**
                 * [Char]
                 * Multiplication
                 */
                        CHAR_MULTIPLICATION,
                /**
                 * [Char]
                 * Division
                 */
                        CHAR_DIVISION,

                /* Indicators */
                /**
                 * Function indicator
                 */
                        INDICATOR_FUNCTION,
                /**
                 * Class indicator
                 */
                        INDICATOR_CLASS,

                /**
                 * Variable indicator
                 */
                        INDICATOR_VARIABLE,

                /* Other */
                /**
                 * Label
                 * [-> Variable, Class and Function name]
                 */
                        LABEL
            } ASTType;

            /**
             * Method to convert an ASTType to a String.
             *
             * @param value the ASTType to convert
             * @return the String value of the ASTType
             */
            String GetASTTypeName(const ASTType value) {
//                const char *typeName = nullptr;
                String typeName;
#define GET_ENUM_NAME(val) \
                case (val):            \
                    typeName = #val;   \
                    break;
                switch (value) {
                    // Default value
                    GET_ENUM_NAME(UNDEFINED);

                    // Special Types
                    GET_ENUM_NAME(SINGLE_LINE_COMMENT);
                    GET_ENUM_NAME(MULTI_LINE_COMMENT);
                    GET_ENUM_NAME(STRING);
                    GET_ENUM_NAME(SOURCE);

                    // Chars
                    GET_ENUM_NAME(CHAR_CURLY_BRACKET_OPEN);
                    GET_ENUM_NAME(CHAR_CURLY_BRACKET_CLOSE);
                    GET_ENUM_NAME(CHAR_ROUND_BRACKET_OPEN);
                    GET_ENUM_NAME(CHAR_ROUND_BRACKET_CLOSE);
                    GET_ENUM_NAME(CHAR_SEMICOLON);
                    GET_ENUM_NAME(CHAR_DOT);
                    GET_ENUM_NAME(CHAR_COMMA);
                    GET_ENUM_NAME(CHAR_COLON);

                    // Mathematical
                    GET_ENUM_NAME(CHAR_ADDITION);
                    GET_ENUM_NAME(CHAR_SUBSTRACTION);
                    GET_ENUM_NAME(CHAR_MULTIPLICATION);
                    GET_ENUM_NAME(CHAR_DIVISION);

                    // Indicator
                    GET_ENUM_NAME(INDICATOR_FUNCTION);
                    GET_ENUM_NAME(INDICATOR_CLASS);
                    GET_ENUM_NAME(INDICATOR_VARIABLE);

                    // Other
                    GET_ENUM_NAME(LABEL);
                }
#undef GET_ENUM_NAME
                return typeName;
            }

            /**
             * Stream operator
             *
             * @param out the original stream
             * @param ASTType the ASTType to convert
             * @return the modified stream
             */
            std::ostream &operator<<(std::ostream &out, const ASTType astType) {
                return out << GetASTTypeName(astType);
            }

            /**
             * [String] Stream operator
             *
             * @param out the original String
             * @param ASTType the ASTType to convert
             * @return the modified String
             */
            String &operator<<(String &out, const ASTType astType) {
                return out += GetASTTypeName(astType);
            }

            /**
             * String 'plus equals' operator
             *
             * @param out the original String
             * @param ASTType the ASTType to convert
             * @return the modified String
             */
            String &operator+=(String &out, const ASTType astType) {
                return out += GetASTTypeName(astType);
            }

            /* AST Object definition */
            /**
             * ASTObject
             * Holds the actually ASTType and it's corresponding value (which can be null!)
             */
            struct ASTObject {
                ASTType type;
                String value;
            };

            /**
             * Covert an ASTObject to a String.
             *
             * @param astObject the ASTObject to convert
             * @return the resulting string
             */
            String ASTObjectToString(ASTObject astObject, bool encoded = false) {
                if (astObject.value.size() > 0) {
                    if (encoded) {
                        return std::to_string((int) astObject.type) + "{" + astObject.value + "}";
                    } else {
                        return "[" + GetASTTypeName(astObject.type) + "]{" + astObject.value + "}";
                    }
                } else {
                    if (encoded) {
                        return std::to_string((int) astObject.type) + "";
                    } else {
                        return "[" + GetASTTypeName(astObject.type) + "]";
                    }
                }
            }

            /**
             * Stream operator
             *
             * @param out the original stream
             * @param ASTObject the ASTObject to convert
             * @return the modified stream
             */
            std::ostream &operator<<(std::ostream &out, const ASTObject astObject) {
                return out << ASTObjectToString(astObject);
            }

            /**
             * [String] Stream operator
             *
             * @param out the original String
             * @param ASTObject the ASTObject to convert
             * @return the modified String
             */
            String &operator<<(String &out, const ASTObject astObject) {
                return out += ASTObjectToString(astObject);
            }

            /**
             * String 'plus equals' operator
             *
             * @param out the original String
             * @param ASTObject the ASTObject to convert
             * @return the modified String
             */
            String &operator+=(String &out, const ASTObject astObject) {
                return out += ASTObjectToString(astObject);
            }

            bool operator<(ASTObject first, ASTObject second) {
                return false;
            }
        }
    }
}

typedef slang::model::ast::ASTTypes ASTType;
typedef slang::model::ast::ASTObject ASTObject;

#endif //SLANG_MODEL_AST_HPP

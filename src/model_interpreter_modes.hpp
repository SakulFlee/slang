/*
 * SLANG - Test (Test.cpp)
 * Copyright (C) 2017 Lukas | @Sakul6499 [Sakul6499@live.de]
 * Created on 29.05.2017.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SLANG Interpreter Modes
 * Defines the target to be processed.
 */
#ifndef SLANG_MODEL_INTERPRETER_MODES_HPP
#define SLANG_MODEL_INTERPRETER_MODES_HPP

// Default Includes
#include "defs.h"

/**
 * Default SLANG namespace
 */
namespace slang {
    /**
     * Model namespace
     */
    namespace model {
        /**
         * Modes namespace
         */
        namespace modes {
            /**
             * Modes enum
             * Includes all working modes.
             * [Used as an argument and stored within global settings]
             */
            typedef enum Modes {
                /* Default */
                /**
                 * Undefined value [default]
                 */
                        UNDEFINED = 0,

                /* Target: AST [low level] */
                /**
                 * Create an AST Output by labels
                 */
                        LITERAL_AST,
                /**
                 * Create an AST Output by bytes
                 */
                        ENCODED_AST,

                /* Target: Source [high level] */
                /**
                 * Create an C++ Source output
                 */
                        SOURCE_CPP,

                /* Target: Binary [higher level] */
                /**
                 * Create a C++ Binary output
                 * [Note: C++ Compiler required!]
                 * TODO [Note: NOT IMPLEMENTED YET]
                 */
                        BINARY_CPP,
                /**
                 * Create a SLANG Binary output
                 * TODO [Note: NOT IMPLEMENTED YET]
                 */
                        BINARY_SLANG
            } Mode;

            /**
             * Method to convert a Mode into a String.
             *
             * @param value the mode to convert
             * @return the name of given Mode value as a String
             */
            String GetModeName(const Mode value) {
                String modeName;
#define GET_ENUM_NAME(val) \
                case (val):            \
                    modeName = #val;   \
                    break;
                switch (value) {
                    GET_ENUM_NAME(UNDEFINED);
                    GET_ENUM_NAME(LITERAL_AST);
                    GET_ENUM_NAME(ENCODED_AST);
                    GET_ENUM_NAME(SOURCE_CPP);
                    GET_ENUM_NAME(BINARY_CPP);
                    GET_ENUM_NAME(BINARY_SLANG);
                }
#undef GET_ENUM_NAME
                return modeName;
            }

            /**
             * Stream operator
             *
             * @param out the original stream
             * @param mode the mode to convert
             * @return the modified stream
             */
            std::ostream &operator<<(std::ostream &out, const Mode mode) {
                return out << GetModeName(mode);
            }

            /**
             * [String] Stream operator
             *
             * @param out the original String
             * @param mode the mode to convert
             * @return the modified String
             */
            String &operator<<(String &out, const Mode mode) {
                return out += GetModeName(mode);
            }

            /**
             * String 'plus equals' operator
             *
             * @param out the original String
             * @param mode the mode to convert
             * @return the modified String
             */
            String &operator+=(String &out, const Mode mode) {
                return out += GetModeName(mode);
            }
        }
    }
}
#endif //SLANG_MODEL_INTERPRETER_MODES_HPP

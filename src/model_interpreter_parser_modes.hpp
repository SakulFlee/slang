/*
 * SLANG - model_ast (model_ast.hpp) 
 * Copyright (C) 2017 Lukas | @Sakul6499 [Sakul6499@live.de]
 * Created on 31.05.2017.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Model AST definition
 */
#ifndef SLANG_MODEL_INTERPRETER_PARSER_MODES_HPP
#define SLANG_MODEL_INTERPRETER_PARSER_MODES_HPP

/**
 * Default SLANG namespace
 */
namespace slang {
    /**
     * Model namespace
     */
    namespace model {
        /**
         * Interpreter-Parser Mode namespace
         */
        namespace interpreter_parser_modes {
            /**
             * ASTTypes enum
             * Includes all (valid) AST Types.
             * Note: Encoded AST makes use of order
             */
            typedef enum ParserModes {
                /* Default */
                /**
                 * Undefined value [default]
                 */
                        NORMAL_MODE = 0,

                // TODO: doc
                        SINGLE_LINE_COMMENT_MODE,
                MULTI_LINE_COMMENT_MODE,
                STRING_MODE,
                SOURCE_MODE
            } ParserMode;

            String GetParserModeName(const ParserMode value) {
                String modeName;

#define GET_ENUM_NAME(val)              \
                case(val):              \
                    modeName = #val;    \
                    break;

                switch (value) {
                    // Default value
                    GET_ENUM_NAME(NORMAL_MODE);

                    /* Special Modes */
                    GET_ENUM_NAME(SINGLE_LINE_COMMENT_MODE);
                    GET_ENUM_NAME(MULTI_LINE_COMMENT_MODE);
                    GET_ENUM_NAME(STRING_MODE);
                    GET_ENUM_NAME(SOURCE_MODE);
                }
#undef GET_ENUM_NAME

                return modeName;
            }

            std::ostream &operator<<(std::ostream &out, const ParserMode parserMode) {
                return out << GetParserModeName(parserMode);
            }

            String &operator<<(String out, const ParserMode parserMode) {
                return out += GetParserModeName(parserMode);
            }

            String &operator+=(String out, const ParserMode parserMode) {
                return out += GetParserModeName(parserMode);
            }
        }
    }
}

typedef slang::model::interpreter_parser_modes::ParserModes ParserMode;

#endif //SLANG_MODEL_INTERPRETER_PARSER_MODES_HPP
